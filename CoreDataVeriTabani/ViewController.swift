//
//  ViewController.swift
//  CoreDataVeriTabani
//
//  Created by Kerim Çağlar on 07/08/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Core Data Kullanımı
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext //persistent OLACAK
        
        let users = NSEntityDescription.insertNewObject(forEntityName:"Users", into:context)
        
//        users.setValue("Kerim",forKey:"name")
//        users.setValue("123456", forKey:"password")
//        users.setValue(10, forKey:"age")
//        users.setValue("info@kerimcaglar.com", forKey:"email")
//
//        do {
//            try context.save()
//            print("Kaydedildi")
//        } catch  {
//            print("Bir Hata Oluştu")
//        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName:"Users")
        //request.predicate = NSPredicate(format: "email= %@", "info1@kerimcaglar.com")
        
        do {
            let users = try context.fetch(request)
            if users.count > 0{
                for user in users as! [NSManagedObject]{
                    
//                    context.delete(user)
//                    
//                    do{
//                        try context.save()
//                        
//                    }catch{
//                        print("Silme İşlemi Başarı ile Gerçekleştirildi")
//                    }
                    
                    if let name = user.value(forKey: "email") as? String {
                        print(name)
                    }
                }
            }else{
                print(".....")
            }
            
        } catch  {
            print("Veri Çekmede Hata Meydana Geldi")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

